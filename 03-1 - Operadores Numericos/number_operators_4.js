//Operador | OR
// 4 | 3;
//7
(4).toString(2)
//'100'
(3).toString(2)
//'11'
(4).toString(2).padStart(32,0)
//'00000000000000000000000000000100'
(3).toString(2).padStart(32,0)
//'00000000000000000000000000000011'

//00000000000000000000000000000100
//00000000000000000000000000000011
//----------------------------------
//00000000000000000000000000000111

//0b111
//7

//Operador & AND
// 3 & 1;
//1
//(3).toString(2).padStart(32,0)
//'00000000000000000000000000000011'
//(1).toString(2).padStart(32,0)
//'00000000000000000000000000000001'
//---------------------------------
//0000001

//Operador ^ XOR = Somente 1 com 0 = 1
// 5 ^ 2;

//Operador ~ NOT = inverte todos os bits, incluindo o de sinal
// ~2;
//-3
//(2).toString(2).padStart(32,0)
//'00000000000000000000000000000010'
//(-3 >>> 0).toString(2).padStart(32,0) // se tentar converter -3, irá mostrar -11 no resultado. neste caso usa a rotação de bits >>>

//Operador << DESLOCAR PARA ESQUERDA =  como se estivesse multiplicando o numero por dois
// 4 << 2;
//16
//(4).toString(2).padStart(32,0)
// '00000000000000000000000000000100'
//(16).toString(2).padStart(32,0)
// '00000000000000000000000000010000'

//Operador >> deslocar a direita = como se estivesse divindo por dois
// 128 >> 1;
//64
//(128).toString(2).padStart(32,0)
// '00000000000000000000000010000000'
//(64).toString(2).padStart(32,0)
// '00000000000000000000000001000000'

//Operador rotacao que leva sinal junto
// -2 >>> 1;
//2147483647

//(-2 >>> 0).toString(2).padStart(32,0)
// '11111111111111111111111111111110'
//(2147483647).toString(2).padStart(32,0)
// '01111111111111111111111111111111'


