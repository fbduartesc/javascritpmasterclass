//Pratica tipo de dados
//Tipos primitivos

typeof 10;
typeof "JavaScript";
typeof true;
typeof Symbol("iterator");
typeof null;
typeof undefined;

//Saidas
/*
> typeof 10;
'number'
> typeof "JavaScript";
'string'
> typeof true;
'boolean'
> typeof Symbol("iterator");
'symbol'
> typeof null;
'object'
> typeof undefined;
'undefined'
*/

(10).toFixed(2);
('JavaScript').replace('a', '4');
(true).toString();
(Symbol('iterator')).toString();

/*
> (10).toFixed(2)
'10.00'
> ('JavaScript').replace('a', '4');
'J4vaScript'
> (true).toString();
'true'
> (Symbol("iterator")).toString();
'Symbol(iterator)'
*/