//Pratica tipo de dados
//Objeto sao valores que representam uma referencia em memoria que pode ser alterada

typeof function sum(a,b){return a+b};
typeof {name: 'Linus Torvalds'};
typeof [1,2,3,4,5,6];
typeof /[a-zA-Z]+/;
typeof (new Date());

//Saidas
/*
> typeof function sum(a,b){return a+b};
'function'
> typeof {name: "Linus Torvaldas"};
'object'
> typeof [1,2,3,4,5,6];
'object'
> typeof /[a-zA-Z]+/;
'object'
> typeof (new Date());
'object'
*/