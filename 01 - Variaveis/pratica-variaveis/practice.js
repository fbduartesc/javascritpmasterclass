/*
Ciclo vida da variavel

- Declaracao
- Inicializacao
- Atribuicao

*/

/*
//01
console.log('######## Exemplo 01 #######');
var idade = 59;
console.log(idade);

//02
console.log('######## Exemplo 02 #######');
console.log(ano);
var ano = 2019;

//03
console.log('######## Exemplo 03 #######');
var pi =3.14;
console.log(pi);
pi = 3;
console.log(pi);

//04
console.log('######## Exemplo 04 #######');
var pi = 3.14;
console.log(pi);
var pi = 3;
console.log(pi);

//05
console.log('######## Exemplo 05 #######');
if(true){
    var pi = 3.14;
}
console.log(pi);

//06
console.log('######## Exemplo 06 #######');
let pi = 3.14;
console.log(pi);


//07
console.log('######## Exemplo 07 #######');
console.log(pi);
let pi = 3.14;


//08
console.log('######## Exemplo 08 #######');
let pi = 3.14;
console.log(pi);
pi = 3;
console.log(pi);


//09
console.log('######## Exemplo 09 #######');
let pi = 3.14;
console.log(pi);
let pi = 3;
console.log(pi);

//10
console.log('######## Exemplo 10 #######');
if(true){
    let pi = 3.14;
}
console.log(pi);

//11
console.log('######## Exemplo 11 #######');
const pi = 3.14;
console.log(pi);

//12
console.log('######## Exemplo 12 #######');
console.log(pi);
const pi = 3.14;


//13
console.log('######## Exemplo 13 #######');
const pi = 3.14;
console.log(pi);
pi = 3;
console.log(pi);

//14
console.log('######## Exemplo 14 #######');
const pi = 3.14;
console.log(pi);
const pi = 3;
console.log(pi);

//15
console.log('######## Exemplo 15 #######');
if(true){
    const pi = 3.14;
}
console.log(pi);
 */

//16
console.log('######## Exemplo 16 #######');
(function(){
    pi = 3.14;
})();
console.log(pi);

let name;
let Name;
let _name;
let $name;

/*
let 123name;
let @Name;
let ''_name;
let "$name;
*/
